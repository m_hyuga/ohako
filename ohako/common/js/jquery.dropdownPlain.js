﻿$(function(){

    $("ul.dropdown li").hover(function(){

        // $(this).addClass("hover");
        // $('ul:first',this).css('visibility', 'visible');
        $(this).children('.sub_menu').stop(true,false).fadeIn();

    }, function(){

        // $(this).removeClass("hover");
        // $('ul:first',this).css('visibility', 'hidden');
        $(this).children('.sub_menu').stop(true,false).fadeOut();

    });

    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");

});