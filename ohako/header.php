<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
<meta name="keywords" content="金沢にある「シェア・スタジオ」OHAKO">
<meta name="description" content="金沢にある「シェア・スタジオ」OHAKO">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<?php if(is_home()): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css">
<?php endif; ?>
<?php if(get_post_type() == 'works'): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/works_archaive.css">
<?php endif; ?>
<?php if(get_post_type() == 'archives'): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/archives.css">
<?php endif; ?>
<?php if(is_singular('archives')): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/works.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/editor-style.css">
<?php endif; ?>
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.dropdownPlain.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/skrollr.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<script>
$(function(){$(".colorbox").colorbox({maxWidth:'100%',maxHeight:'100%',speed:'200'});});
</script>
<script type="text/javascript">
	$(function(){
		var s = skrollr.init();
	});
</script>
</head>

<body>
<div id="wrapper">
	<header id="header">
		<div class="hinner">
			<h1 id="logo"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.gif" alt="OHAKO STUDIO"></h1>
			<ul id="navi" class="clearfix">
				<li><a href="<?php bloginfo('url'); ?>/#aboutus" class="al"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01.gif" alt="About us"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/archives/" class="al"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02.gif" alt="Archaives"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/works/" class="al"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03.gif" alt="Works"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/#information" class="al"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04.gif" alt="Information"></a></li>
				<li class="pt00"><a href="mailto:info@ohakostudio.com" class="al"><img src="<?php bloginfo('template_url'); ?>/common/images/mail_btn.gif" alt="mail"></a></li>
			</ul>
		</div>
	</header><!-- //#header -->

	<div id="contents">
