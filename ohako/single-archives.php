<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
if (!is_user_logged_in()) {
    wp_safe_redirect( get_home_url() );}

get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<script type="text/javascript">
	$(function(){
		$('#thm li').click(function(){
			var Index = $('#thm li').index(this);//クリックされた要素が何番目かを変数に代入
			$('#pic li').css({'display':'none'});//一度拡大画像は全て非表示
			$('#pic li').eq(Index).css({'display':'block'});//クリックされた要素と同じ順番の拡大画像を表示
		});
	});
	</script>
<section>
<h1 class="headTitle"><span><img src="<?php bloginfo('template_url'); ?>/images/archaive/ttl.png" alt="Archaives"></span></h1>
<div class="inner clearfix">
	<div class="mainCont">
		<p class="headTitle02 fo18"><?php the_title(); ?></p>
		<div class="worksDetailArea">
			<ul id="pic">
				<?php
				echo '<li class="first"><img src="';
				$image_id = SCF::get('img_main');
				$image = wp_get_attachment_image_src($image_id, 'large');
					if (!empty($image_id)) {echo $image[0];
					}
				echo '" alt=""></li>';
				$repeat_group = scf::get('img_other');
				$cat_cnt = 0;
				foreach ( $repeat_group as $field_name => $field_value ) :
				$cat_cnt++;

				echo '<li><img src="';
				$val =  $field_value["img_list"];
					if (empty($val)) {
						echo '';
					} else {
					$image = wp_get_attachment_image_src($val, 'large');echo $image[0];
					}
				echo '" alt=""></li>';
				 ?>
				<?php endforeach; ?>
			</ul>


			<div class="mceContentBody">
			<?php the_content(); ?>
			</div>
			<ul id="thm" class="img">
				<?php
				$for_cnt = 0;
				$repeat_group = scf::get('img_other');
				foreach ( $repeat_group as $field_name => $field_value ) :
				$for_cnt++;
				$val =  $field_value["img_list"];
				if (!$val == ''){
				if ($for_cnt == 1 ):
				?>
					<li><span class="thumb_img" style="background-image: url('<?php	$image_id = SCF::get('img_main');
						$image = wp_get_attachment_image_src($image_id, 'medium');
						if (!empty($image_id)){echo $image[0];};
					?>')"></span></li>
					<?php endif;};	?>
						<li><span class="thumb_img" style="background-image: url('<?php 
					$val =  $field_value["img_list"];
						if (!empty($val)) {
						$image = wp_get_attachment_image_src($val, 'medium');echo $image[0];
						}
					 ?>')"></span></li>
				<?php endforeach; ?>
			</ul>
			
			<p class="re_top"><a href="<?php bloginfo('url'); ?>/archives/">Archivesトップへ戻る</a></p>
		</div>
	</div>
</div>
</section>



<?php endwhile; endif; ?>
<?php get_footer(); ?>
