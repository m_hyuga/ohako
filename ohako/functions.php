<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = twentyfifteen_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'common/css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentyfifteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';







// ワードプレスへようこそを非表示
remove_action( 'welcome_panel', 'wp_welcome_panel' );
// ------投稿一覧非表示 end
function example_remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'] ); //アクティビティ
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

add_action( 'wp_before_admin_bar_render', 'my_wp_before_admin_bar_render' );
function my_wp_before_admin_bar_render() {
   global $wp_admin_bar;
   $wp_admin_bar->remove_menu('updates');  // 更新
   $wp_admin_bar->remove_menu('comments');  // コメント
   $wp_admin_bar->remove_menu('new-content');  // 新規
   $wp_admin_bar->remove_menu('user-info');  // マイアカウント内「プロフィール」
   $wp_admin_bar->remove_menu('edit-profile');  // マイアカウント内「プロフィールを編集」
}

function my_manage_posts_columns($columns) {
    // unset($columns['cb']); // チェックボックス
    // unset($columns['title']); // 投稿タイトル
     unset($columns['author']); // 投稿者名
     unset($columns['categories']); // カテゴリ
     unset($columns['tags']); // タグ
     unset($columns['comments']); // コメント
    // unset($columns['date']); // 投稿日時
    return $columns;
}
add_filter('manage_posts_columns', 'my_manage_posts_columns');
function my_manage_pages_columns($columns) {
    // unset($columns['cb']); // チェックボックス
    // unset($columns['title']); // タイトル
    unset($columns['author']); // 作成者名
    unset($columns['comments']); // コメント
    // unset($columns['date']); // 作成日時
    return $columns;
}
add_filter( 'manage_pages_columns', 'my_manage_pages_columns' );
// ---通常post
function remove_default_post_screen_metaboxes() {
    remove_meta_box('postcustom', 'post', 'normal'); // カスタムフィールド
    remove_meta_box('postcustom', 'page', 'normal'); // カスタムフィールド
    remove_meta_box('postexcerpt', 'post', 'normal'); // 抜粋
		remove_meta_box('postimagediv', 'post', 'side'); // Publish
		
    remove_meta_box('commentstatusdiv', 'post', 'normal'); // コメント設定
    remove_meta_box('commentsdiv' , 'page' , 'normal' ); /* 固定ページのコメント */
    remove_meta_box('trackbacksdiv', 'post', 'normal'); // トラックバック設定
    remove_meta_box('revisionsdiv', 'post', 'normal'); // リビジョン表示
    remove_meta_box('formatdiv', 'post', 'normal'); // フォーマット設定
    remove_meta_box('slugdiv', 'post', 'normal'); // スラッグ設定
    remove_meta_box('slugdiv', 'page', 'normal'); // スラッグ設定
    remove_meta_box('authordiv', 'post', 'normal'); // 投稿者
    remove_meta_box('authordiv', 'page', 'normal'); // 投稿者
		remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' ); /* 投稿のディスカッション */
		remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' ); /* 投稿のディスカッション */
    remove_meta_box('categorydiv', 'post', 'normal'); // カテゴリー
    remove_meta_box('tagsdiv-post_tag', 'post', 'normal'); // タグ
		
 }
add_action('admin_menu','remove_default_post_screen_metaboxes');
function remove_menu() {
		remove_submenu_page('index.php', 'update-core.php'); // ダッシュボード -> 更新
		remove_menu_page('edit.php');
//		remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
//		remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category'); // 投稿 -> カテゴリ
    remove_menu_page('link-manager.php'); // リンク
    remove_menu_page('edit-comments.php'); // コメント
		//remove_submenu_page('plugins.php', 'plugin-editor.php'); // プラグイン -> プラグイン編集
		//remove_menu_page('users.php'); // ユーザー
		//remove_menu_page('tools.php'); // ツール
}
add_action('admin_menu', 'remove_menu');


// バージョン更新を非表示にする
add_filter('pre_site_transient_update_core', '__return_zero');
// フッターWordPressリンクを非表示に
function custom_admin_footer() {
 echo '';
 }
add_filter('admin_footer_text', 'custom_admin_footer');





if (is_main_site()){
	add_action( 'init', 'create_post_type' );
	function create_post_type() {
		// Archives
		register_post_type( 'archives', /*  */
			array(
				'labels' => array(
				'name' => __( 'Archives' ), /* ダッシュボードに表示する名前 */
				'singular_name' => __( 'Archives' ) /* 「name」と同じに */
				),
				'menu_position' => 6,
				'has_archive' => true,
				'public' => true, /* サイトで公開するかどうか */
				'supports' => array( 'title', 'editor' ), /* サポートする機能 */
			)
		);
		// Works
		register_post_type( 'works', /*  */
			array(
				'labels' => array(
				'name' => __( 'Works' ), /* ダッシュボードに表示する名前 */
				'singular_name' => __( 'Works' ) /* 「name」と同じに */
				),
				'menu_position' => 7,
				'has_archive' => true,
				'public' => true, /* サイトで公開するかどうか */
				'supports' => array( 'title', 'editor' ), /* サポートする機能 */
			)
		);
	}
};


// ------投稿画面非表示
add_action('admin_print_styles', 'admin_book_css_custom');
function admin_book_css_custom() {
global $typenow;
if($typenow == 'archives' or $typenow == 'works'):
echo '<style>#postdivrich {display: none;}</style>';
endif;
}



// ----------head内きれい
add_filter( 'show_admin_bar', '__return_false' );
remove_action('wp_head','wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
// ----------head内きれい end

// ------投稿画像の<a>タグにポップアップ用クラスを追加
function give_linked_images_class( $html, $id, $caption, $title, $align, $url, $size ) {
  $str_a = '<a href';
  if(strstr($html, $str_a)){
    $str_a_class = '<a class="colorbox" href';
    $html = str_replace($str_a, $str_a_class, $html);
    $str_img = '" /></a>';
    $str_img_class = ' over" /></a>';
    $html = str_replace($str_img, $str_img_class, $html);
  }
return $html; 
} 
  
add_action( 'image_send_to_editor', 'give_linked_images_class', 10 ,8);


////固定ページの新規追加と編集ではWYSIWYGを使用しない
function dis_post_wysiwyg(){
  //初期化
  $post = null;
  $post_type_object = null;
  $post_type = null;
  $post_id = null;
  if ( isset($_GET['post']) ) {
    $post_id = (int) $_GET['post'];
    if ( $post_id ) {
      $post = get_post($post_id);
      if ( $post ) {
        $post_type_object = get_post_type_object($post->post_type);
        if ( $post_type_object ) {
          $post_type = $post->post_type;
        }
      }
    }
  }
  if ( isset($_GET['post_type']) ) {
    $post_type = $_GET['post_type'];
  }
  if ( $post_type == 'page' ) {
    add_filter('user_can_richedit' , create_function('' , 'return false;') , 50);
  }
}
add_action('admin_init', 'dis_post_wysiwyg');




// -----------ページャー
function pagination($pages = '', $range = 2) {
$categories = get_the_category();
$catid = '';
if ( !$categories->cat_ID ) $catid = '&cat='.$categories->cat_ID;
     $showitems = ($range * 1)+1;
     global $paged;
     if(empty($paged)) $paged = 1;
      if($pages == '') {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages) { $pages = 1; }}
     if(1 != $pages) {
         if($paged > 1) { echo "<ul><li class='prev'><a href='".get_pagenum_link($paged - 1)."' >prev</a></li>\n<li class='pagenation'><a href='".get_pagenum_link(1)."'>1</a></li>\n<li>...</li>\n"; }else {echo "<ul>";}
          for ($i=1; $i <= $pages; $i++) {
        if ($paged <= $range) $gap = $range-$paged+1;
        if ($pages < $paged+$range ) $gap = $range-($pages-$paged);
             if (1 != $pages &&( !($i >= $gap+$paged+$range+1 || $i <= $paged-$range-$gap-1) || $pages <= $showitems)) {
                 echo ($paged == $i)? "<li class='off'>".$i."</li>\n":"<li class='pagenation' onclick='location.href=\"".get_pagenum_link($i)."\"' title='".$i."ページ'><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
             }}
         if ($paged < $pages) {  echo "<li>...</li>\n<li class='pagenation' onclick='location.href=\"".get_pagenum_link($pages)."\"'><a href='".get_pagenum_link($pages)."'>".$pages."</a></li>\n<li class='next'><a href=\"".get_pagenum_link($paged + 1)."\" >next</a></li>\n</ul>";
           }else{echo "</ul>";}
     }}
// -----------ページャー end



/* テンプレートフォルダのパスをショートコードに登録
--------------------------------------------------------- */
function shortcode_templateurl() {
    return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');


function homepage_url() {
    return home_url();
}
add_shortcode('home_url', 'homepage_url');


function PHP_Include($params = array()) {
	extract(shortcode_atts(array(
	    'file' => 'default'
	), $params));
	ob_start();
	include(get_theme_root() . '/' . get_template() . "/$file.php");
	return ob_get_clean();
}
// register shortcode
add_shortcode('tp', 'PHP_Include');




// アーカイブログイン
add_action( 'auth_redirect', 'subscriber_go_to_home' );
function subscriber_go_to_home( $user_id ) {
	$user = get_userdata( $user_id );
	if ( !$user->has_cap( 'edit_posts' ) ) {
		wp_redirect( get_home_url().'/archives' );
		exit();
	}
}

// ログアウト後のリダイレクト
function redirect_fix(){
	wp_safe_redirect( get_home_url() );
	exit();
}
add_action('wp_logout','redirect_fix');