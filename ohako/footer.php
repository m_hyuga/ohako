<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	</div><!-- //#content -->

	<footer id="footer">
		<small>Copyright (C) 2015 OHAKO. All Rights Reserved.</small>
		<p class="pagetop"><a href="#wrapper"><img src="<?php bloginfo('template_url'); ?>/common/images/gotop.png" alt="gotop"></a></p>
	</footer>
</div><!-- #wrapper -->
<?php wp_footer(); ?>

</body>
</html>
