<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>

<section class="archaivesArea">
	<h1 class="headTitle"><span><img src="<?php bloginfo('template_url'); ?>/images/works/ttl.png" alt="Works"></span></h1>
	<div class="inner">
		<?php if (have_posts()) : ?>
		<ul>
		<?php while (have_posts()) : the_post(); ?>
					<li><a href="<?php	$image_id = SCF::get('img_main');
						$image = wp_get_attachment_image_src($image_id, 'large');
						if (empty($image_id)) {}else{echo $image[0];};
					?>" class="colorbox"><img src="<?php	$image_id = SCF::get('img_main');
						$image = wp_get_attachment_image_src($image_id, 'large');
						if (empty($image_id)) {}else{echo $image[0];};
					?>" alt="Archaives"></a>
								<p><?php the_title(); ?></p></li>
		<?php endwhile; endif; ?>
		</ul>

	</div>
</section>
<div id="page" class="cf">
<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
</div>

<?php get_footer(); ?>
