<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
if (!is_user_logged_in()) {
    wp_safe_redirect( get_home_url() );}

get_header(); ?>
<section class="archaivesArea">
<h1 class="headTitle"><span><img src="<?php bloginfo('template_url'); ?>/images/archaive/ttl.png" alt="Archaives"></span></h1>
<div class="inner">
		<?php if (have_posts()) : ?>
		<ul>
		<?php while (have_posts()) : the_post(); ?>
					<li><a href="<?php the_permalink(); ?>"><span class="thumb_img" style="background-image: url('<?php	$image_id = SCF::get('img_main');
						$image = wp_get_attachment_image_src($image_id, 'medium');
						if (empty($image_id)) {}else{echo $image[0];};
					?>')"></span></a></li>
		<?php endwhile; endif; ?>
		</ul>
</div>
</section>

<div id="page" class="cf">
<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
</div>


<!--<a href="<?php echo wp_logout_url(); ?>">ログアウト</a>-->
<?php get_footer(); ?>
