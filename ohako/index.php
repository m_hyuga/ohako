<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<section class="mainimg" data-0="background-position:50% 0px;" data-810="background-position:50% 100px;">
<!-- <p class="mimg"><img src="<?php bloginfo('template_url'); ?>/images/top/mainimg.jpg" alt="OHAKO STUDIO"></p> -->
<h2 class="mttl"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl01.png" alt="OHAKO STUDIO"></h2>
</section>

<section id="aboutus" class="aboutArea">
<div class="inner">
<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl02.png" alt="About Us"></h2>
<p class="txt fo14">オハコスタジオは、金沢にある「シェア・スタジオ」です。<br>
2012年11⽉に、シェアオフィスのスタジオ版として設⽴されました。現在⼊居者は2名。(設⽴当時からですが、、)<br>
イケダカメラ・池⽥紀幸写真事務所（スタジオ代表）と、フリーランス⼭⽥康太が所属しています。<br>
主な業務は、「広告・商業写真の撮影」「CM、PV 等動画撮影」（⼭⽥のみ）で、<br>
ポートレイト、ファッション、スチルライフ、料理、建築等幅広く撮影しています。</p>
<ul class="dropdown clearfix">
	<li><a href="#" class="li01"><span class="names">池田紀幸</span></a>
		<ul class="sub_menu">
			<li><div class="ikedaArea">
				<p class="img"><img src="<?php bloginfo('template_url'); ?>/images/top/ikeda.png" alt="池田紀幸"><span class="names">池田紀幸</span></p>
				<div class="fo14">
					<p>イケダカメラ・池⽥紀幸写真事務所</p>
					<p>スタジオ勤務後、'04年に独⽴、事務所設⽴。'12年、オハコスタジオ設⽴。⽯川県写真家協会正会員、金沢アートディレクターズクラブ会員、⽯川県理容美容専⾨学校⾮常勤講師、グローバルビューティーコングレス北陸⼤会審査員、ミスユニバース⽯川⼤会審査員。<br>
■主な受賞歴（写真担当）<br>消費者のためになった広告コンクール⾦賞、銀賞、金沢ADC 準グランプリ、金沢ADC 賞、中島信也賞、佐藤卓賞、はせがわさとし賞、HCC賞、北國広告賞、等。</p>
				</div>
			</div></li>
		</ul>
	</li>
	<li><a href="#" class="li02"><span class="names">山田康太</span></a>
		<ul class="sub_menu">
			<li><div class="yamadaArea">
				<p class="img"><img src="<?php bloginfo('template_url'); ?>/images/top/yamada.png" alt="山田康太"><span class="names">山田康太</span></p>
				<div class="fo14">
					<p>イケダカメラ・池⽥紀幸写真事務所</p>
					<p>スタジオ勤務後、'04年に独⽴、事務所設⽴。'12年、オハコスタジオ設⽴。⽯川県写真家協会正会員、金沢アートディレクターズクラブ会員、⽯川県理容美容専⾨学校⾮常勤講師、グローバルビューティーコングレス北陸⼤会審査員、ミスユニバース⽯川⼤会審査員。<br>
■主な受賞歴（写真担当）<br>消費者のためになった広告コンクール⾦賞、銀賞、金沢ADC 準グランプリ、金沢ADC 賞、中島信也賞、佐藤卓賞、はせがわさとし賞、HCC賞、北國広告賞、等。</p>
				</div>
			</div></li>
		</ul>
	</li>
</ul>
</div>
</section>

<section class="archivesArea">
<div class="inner">
<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl03.png" alt="Archives"></h2>
<div class="archives">
<form method="post" action="<?php echo wp_login_url() ?>?redirect_to=<?php echo esc_attr($_SERVER['REQUEST_URI']) ?>">
	<dl>
		<dt><input type="hidden" name="log" id="login_username" value="member_ohako" /><input type="password" name="pwd" id="login_password" class="text01"></dt>
		<dd><input type="image" src="<?php bloginfo('template_url'); ?>/images/top/btn.gif" alt=""></dd>
	</dl>
</form>
</div>
</div>
</section>

<section id="information" class="informationArea">
<div class="inner clearfix">
<div class="cont">
	<h2><img src="<?php bloginfo('template_url'); ?>/images/top/ttl04.png" alt="Information"></h2>
	<p class="txt fo14">920-1161 ⽯川県⾦沢市鈴⾒台3-15-6<br>
<a href="mailto:info@ohakostudio.com">info@ohakostudio.com</a></p>
	<div class="txt2 fo14">
		<dl>
			<dt>スタジオ⾯積 ：</dt>
			<dd>W8m&times;D11m&times;H5m</dd>
		</dl>
		<dl>
			<dt>駐⾞台数 ：</dt>
			<dd>7台</dd>
		</dl>
		<dl>
			<dt>機材 ：</dt>
			<dd>CANON 1Dx、1Dsmark3、5Dmark3&times;2<br>
MAMIYA645D&amp;67D&times;Phaseone P25plus<br>
PanasonicGH4、BMPCC 等</dd>
		</dl>
	</div>
	<div class="mapBox">
		<div><img src="<?php bloginfo('template_url'); ?>/images/top/map.jpg" alt="大きいマップで見る"></div>
		<p class="fo14 alignRight"><a href="#" target="_blank">大きいマップで見る</a></p>
	</div>
</div>
</div>
</section>




<?php get_footer(); ?>
