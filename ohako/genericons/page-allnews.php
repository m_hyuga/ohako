<?php
/**
* The template for displaying archive pages
*
* Used to display archive-type pages if nothing more specific matches a query.
* For example, puts together date-based pages if no date.php file exists.
*
* If you'd like to further customize these archive views, you may create a
* new template file for each one. For example, tag.php (Tag archives),
* category.php (Category archives), author.php (Author archives), etc.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/

get_header(); ?>
<div id="content" class="news fll">
<div id="inner">
<h3 class="title ttl_index">ニュース</h3>
<ul id="history" class="cf">
	<li><a href="<?php bloginfo('url'); ?>/allnews/news_to_2013/">2011年～2013年</a></li>
	<li><a href="<?php bloginfo('url'); ?>/allnews/news_to_2010/">2007年～2010年</a></li>
	<li><a href="<?php bloginfo('url'); ?>/allnews/news_to_2006/">2005年～2006年</a></li>
</ul>
<h4 class="current_yaer">2014年〜</h4>
<div id="news_list">
	<ul>
		<?php 
		function filter_where($where = '') {
		$now_time = time() - date("Z");
		$now_time = $now_time + 9*3600;
		$now_date = date("Y-m-d H:i:s", $now_time);
		$where .= " AND post_date >= '2014-01-01 00:00:00' AND post_date<'".$now_date."'";
		return $where;
			}
		add_filter('posts_where', 'filter_where');

		query_posts(
			array(
			"paged" => get_query_var('paged'),
			'post_type' => array('news','smalltalk'),
			'tax_query' => array( 
					array(
						'taxonomy'=>'st_tag',
						'terms'=>array( 'hide' ),
						'field'=>'slug',
						'operator'=>'NOT IN'
						),
					array(
						'taxonomy'=>'news_tag',
						'terms'=>array( 'hide' ),
						'field'=>'slug',
						'operator'=>'NOT IN'
						),
					'relation' => 'AND'
					)
			 ) 
		);
		?>
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<?php if (( get_post_type() == 'news')): ?>
			<li><dl><dt><?php the_time('Y/m/d'); ?></dt><dd>-<?php the_title(); ?>
						<?php	
						$val_txt =  scf::get('txt_txt');
						$val_url =  scf::get('txt_url');
						if (empty($val_txt)) {} else {
							if (empty($val_url)) {} else {
								echo '<a href="'.$val_url.'"';
							
								$check =  scf::get('check_brank');
								if ($check == 1) { echo ' target="_blank"'; }
								echo '>';
							}
							echo $val_txt;
							if (empty($val_url)) {} else {
								echo '</a>';
							}
						}
						?></dd></dl></li>
			<?php endif; ?>
			<?php if (( get_post_type() == 'smalltalk')): ?>
			<li><dl><dt><?php the_time('Y/m/d'); ?></dt><dd>-週刊スモールトーク <?php $txt_num =  scf::get('txt_num'); echo $txt_num; ?>話 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dd></dl></li>
			<?php endif; ?>
		<?php endwhile; endif; ?>
	</ul>
</div>

<ul id="pager" class="cf">
			<?php if (function_exists("pagination_prev")) { pagination_prev($additional_loop->max_num_pages); } ?>
			<li><div class="btn_index  center"><a href="<?php bloginfo('url'); ?>/allnews/">最新一覧</a></div></li>
			<?php if (function_exists("pagination_next")) { pagination_next($additional_loop->max_num_pages); } ?>
</ul><!-- /pager -->

<div class="btn_index center-l"><a href="<?php bloginfo('url'); ?>/">Home</a></div>
<?php get_footer(); ?>
